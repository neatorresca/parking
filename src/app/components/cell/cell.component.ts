import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Vehicle } from '../../models/vehicle.model';

@Component({
	selector: 'cell',
	templateUrl: './cell.component.html',
	styleUrls: ['./cell.component.less']
})
export class CellComponent {
	@Input() size: string;
	@Input() id: number;
	@Input() vehicle: Vehicle;
	@Output() release = new EventEmitter<number>();
	
	releaseVehicle(){
		this.release.emit(this.id);
	}
}
