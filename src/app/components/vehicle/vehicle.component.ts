import { Component, Input } from '@angular/core';
import { Vehicle } from '../../models/vehicle.model';

@Component({
  selector: 'vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.less']
})
export class VehicleComponent {
	@Input() vehicle: Vehicle;
}
