import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Cell } from './models/cell.model';
import { Vehicle } from './models/vehicle.model';
import { CellsService } from './services/cells.service';
import { VehiclesService } from './services/vehicles.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit, OnDestroy {
	cells: Cell[];
	queuedVehicles: Vehicle[];
	cancelSubscriptions: Subject<boolean> = new Subject<boolean>();
	types = ['Motorcycle', 'Sedan', 'Truck'];
	emptyCells: Cell[] = [];

	constructor(private _cellsService: CellsService, private _vehiclesService: VehiclesService){}

	ngOnInit(){
		this.cells = this._cellsService.getCells();
		this.queuedVehicles = this._vehiclesService.getVehicles();

		this._vehiclesService.vehiclesChanged.pipe(
			takeUntil(this.cancelSubscriptions)
		).subscribe((data: Vehicle[])=>{
			this.queuedVehicles = data;

			this.emptyCells = this.getEmptyCells();

			if(this.emptyCells.length > 0 && this.queuedVehicles.length > 0){
				this.fillEmptyCells();
			}
		})
	}

	addVehicle(type: string){
		this._vehiclesService.addVehicle(type);

		if(this.emptyCells.length > 0){
			this.fillEmptyCells();
		}
	}

	fillEmptyCells(){
		this.emptyCells.forEach((cell: Cell)=>{
			if(this.queuedVehicles.length > 0 && cell.vehicle === null && this.vehicleMatches(this.queuedVehicles[0].type, cell.size)){
				let index = this.cells.indexOf(cell);
				this.cells[index].vehicle = this.queuedVehicles[0];
				this.emptyCells.splice(index, 1);

				this._vehiclesService.removeVehicle(this.queuedVehicles[0]);
				return;
			}	
		})
	}

	releaseVehicle(cellId: number){
		const currentCell = this.cells.find((cell:Cell)=>{return cell.id === cellId}),
			index = this.cells.indexOf(currentCell);

		this.cells[index].vehicle = null;
		this.emptyCells = this.getEmptyCells();

		this.fillEmptyCells();
	}

	vehicleMatches(vType: string, cSize: string){
		switch(vType){
			case 'Motorcycle':
				return true;
			case 'Sedan':
				return (cSize === 'medium' || cSize === 'large')
			case 'Truck':
				return cSize === 'large'
			default:
				return false
		}
	}

	ngOnDestroy(){
		this.cancelSubscriptions.next(true);
		this.cancelSubscriptions.unsubscribe();
	}

	getEmptyCells(){
		const emptyCells = this.cells.filter((cell: Cell)=> cell.vehicle === null);
		return emptyCells;
	}
}
