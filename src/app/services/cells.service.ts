import { Injectable } from '@angular/core';
import { CELLS } from '../mocks/cells';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CellsService {
	//cellsChanged = new Subject<any>();
	private cells = CELLS;

	constructor() { }

	getCells(){
		return this.cells.slice();
	}
}
