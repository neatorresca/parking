import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Vehicle } from '../models/vehicle.model';

@Injectable({
	providedIn: 'root'
})
export class VehiclesService {
	private vehiclesQueued: Vehicle[] = [];
	private counter: number = 0;
	vehiclesChanged: Subject<Vehicle[]> = new Subject<Vehicle[]>();

	addVehicle(type: string){
		this.vehiclesQueued.push(new Vehicle(++this.counter, type))
		this.vehiclesChanged.next(this.vehiclesQueued.slice());
	}

	getVehicles(){
		return this.vehiclesQueued.slice();
	}

	removeVehicle(vehicle: Vehicle){
		const index = this.vehiclesQueued.indexOf(vehicle);
		this.vehiclesQueued.splice(index, 1);
		this.vehiclesChanged.next(this.vehiclesQueued.slice());
	}
}
