import { Vehicle } from './vehicle.model';

export class Cell{
	constructor(
		public size: string,
		public id: number,
		public vehicle: Vehicle
	){}
}